package com.shoppizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COfficeController {
	@Autowired
	IOfficeRepository officeRepository;

	@GetMapping("/offices")
	public ResponseEntity<List<COffice>> getAllOffices() {
		try {
			List<COffice> pOffices = new ArrayList<COffice>();

			officeRepository.findAll().forEach(pOffices::add);

			return new ResponseEntity<>(pOffices, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/office/{id}")
	public ResponseEntity<COffice> getOfficeById(@PathVariable("id") long id) {
		Optional<COffice> officeData = officeRepository.findById(id);
		if (officeData.isPresent()) {
			return new ResponseEntity<>(officeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/office/create")
	public ResponseEntity<Object> createOffice(@RequestBody COffice pOffice) {
		try {
			COffice newOffice = new COffice();
			newOffice.setCountry(pOffice.getCountry());
			newOffice.setAddressLine(pOffice.getAddressLine());
			newOffice.setCity(pOffice.getCity());
			newOffice.setPhone(pOffice.getPhone());
			newOffice.setState(pOffice.getState());
			newOffice.setTerritory(pOffice.getTerritory());
			COffice savedOffice = officeRepository.save(newOffice);
			return new ResponseEntity<>(savedOffice, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Office: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/office/update/{id}")
	public ResponseEntity<Object> updateOfficeById(@PathVariable("id") long id, @RequestBody COffice pOffice) {
		Optional<COffice> officeData = officeRepository.findById(id);
		if (officeData.isPresent()) {
			COffice newOffice = officeData.get();
			newOffice.setCountry(pOffice.getCountry());
			newOffice.setAddressLine(pOffice.getAddressLine());
			newOffice.setCity(pOffice.getCity());
			newOffice.setPhone(pOffice.getPhone());
			newOffice.setState(pOffice.getState());
			newOffice.setTerritory(pOffice.getTerritory());
			COffice savedOffice = officeRepository.save(newOffice);
			try {
				return new ResponseEntity<>(savedOffice, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Office:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Office: " + id + "  for update.");
		}
	}

	@DeleteMapping("/office/delete/{id}")
	public ResponseEntity<COffice> deleteOfficeById(@PathVariable("id") long id) {
		try {
			officeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/office/city_or_state/{name}")
	public ResponseEntity<List<COffice>> findOfficeByCityOrState(@PathVariable String name) {
		try {
			List<COffice> vOffice = officeRepository.findOfficeByCityOrState(name);
			return new ResponseEntity<>(vOffice, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/office/city_or_state_pageable/{name}/{start}/{end}")
	public ResponseEntity<List<COffice>> findOfficeByCityOrStatePageable(@PathVariable String name, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COffice> vOffice = officeRepository.findOfficeByCityOrStatePageable(name, PageRequest.of(start, end));
			return new ResponseEntity<>(vOffice, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/office/city_or_state_desc/{name}/{start}/{end}")
	public ResponseEntity<List<COffice>> findOfficeByCityOrStateDesc(@PathVariable String name, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COffice> vOffice = officeRepository.findOfficeByCityOrStateDESC(name, PageRequest.of(start, end));
			return new ResponseEntity<>(vOffice, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/office/update_for_country/{country}")
	public int updateCountry(@PathVariable("country") String country) {
		return officeRepository.updateCountry(country);
	}
}
