package com.shoppizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@RestController
public class COrderDetailController {
	@Autowired
	private IOrderDetailRepository orderDetailRepository;

	@GetMapping("/orderdetail/quantity_order/{quantity}")
	public ResponseEntity<List<COrderDetail>> findOrderDetailByQuantityOrder(@PathVariable long quantity) {
		try {
			List<COrderDetail> vOrderDetail = orderDetailRepository.findOrderDetailByQuantityOrder(quantity);
			return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/orderdetail/quantity_order_pageable/{quantity}/{start}/{end}")
	public ResponseEntity<List<COrderDetail>> findOrderDetailByQuantityOrderPageable(@PathVariable long quantity, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COrderDetail> vOrderDetail = orderDetailRepository.findOrderDetailByQauntityOrderPageable(quantity, PageRequest.of(start, end));
			return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/orderdetail/quantity_order_desc/{status}/{start}/{end}")
	public ResponseEntity<List<COrderDetail>> findOrderDetailByQuantityOrderDESC(@PathVariable long quantity, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COrderDetail> vOrderDetail = orderDetailRepository.findOrderDetailByQuantityOrderDESC(quantity, PageRequest.of(start, end));
			return new ResponseEntity<>(vOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/orderdetail/update_for_quantity/{quantity}")
	public int updateQuantityOrder(@PathVariable("quantity") long quantity) {
		return orderDetailRepository.updateQuantityOrder(quantity);
	}
}
