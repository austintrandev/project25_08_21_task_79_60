package com.shoppizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CProductLineController {
	@Autowired
	IProductLineRepository productLineRepository;

	@GetMapping("/productlines")
	public ResponseEntity<List<CProductLine>> getAllProductLines() {
		try {
			List<CProductLine> pProductLines = new ArrayList<CProductLine>();

			productLineRepository.findAll().forEach(pProductLines::add);

			return new ResponseEntity<>(pProductLines, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/productline/{id}")
	public ResponseEntity<CProductLine> getProductLineById(@PathVariable("id") long id) {
		Optional<CProductLine> productLineData = productLineRepository.findById(id);
		if (productLineData.isPresent()) {
			return new ResponseEntity<>(productLineData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/productline/create")
	public ResponseEntity<Object> createProductLine(@RequestBody CProductLine pProductLine){
		try {
			CProductLine newProductLine = new CProductLine();
			newProductLine.setProductLine(pProductLine.getProductLine());
			newProductLine.setDescription(pProductLine.getDescription());
			CProductLine savedProductLine = productLineRepository.save(newProductLine);
			return new ResponseEntity<>(savedProductLine, HttpStatus.CREATED);
		}catch (Exception  e) {
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProdutLine: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/productline/update/{id}")
	public ResponseEntity<Object> updateProductLineById(@PathVariable("id") long id, @RequestBody CProductLine pProductLine) {
		Optional<CProductLine> productLineData = productLineRepository.findById(id);
		if (productLineData.isPresent()) {
			CProductLine newProductLine = productLineData.get();
			newProductLine.setProductLine(pProductLine.getProductLine());
			newProductLine.setDescription(pProductLine.getDescription());
			CProductLine savedProductLine = productLineRepository.save(newProductLine);
			try {
				return new ResponseEntity<>(savedProductLine, HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified ProductLine:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified ProductLine: "+id + "  for update.");
		}
	}
	

	@DeleteMapping("/productline/delete/{id}")
	public ResponseEntity<CProductLine> deleteProductLineById(@PathVariable("id") long id) {
		try {
			productLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/productline/product_line/{line}")
	public ResponseEntity<List<CProductLine>> findProductLineByLine(@PathVariable String line) {
		try {
			List<CProductLine> vProductLine = productLineRepository.findProductLineByLine(line);
			return new ResponseEntity<>(vProductLine, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/productline/product_line_pageable/{line}/{start}/{end}")
	public ResponseEntity<List<CProductLine>> findProductLineByLinePageable(@PathVariable String line, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CProductLine> vProductLine = productLineRepository.findProductLineByLinePageable(line, PageRequest.of(start, end));
			return new ResponseEntity<>(vProductLine, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/productline/product_line_desc/{line}/{start}/{end}")
	public ResponseEntity<List<CProductLine>> findProductLineByLineDESC(@PathVariable String line, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CProductLine> vProductLine = productLineRepository.findProductLineByLineDESC(line, PageRequest.of(start, end));
			return new ResponseEntity<>(vProductLine, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/productline/update_for_product_line/{line}")
	public int updateProductLine(@PathVariable("line") String line) {
		return productLineRepository.updateProductLine(line);
	}
}
