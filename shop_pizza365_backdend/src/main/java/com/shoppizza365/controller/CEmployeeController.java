package com.shoppizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CEmployeeController {

	@Autowired
	IEmployeeRepository employeeRepository;

	@GetMapping("/employees")
	public ResponseEntity<List<CEmployee>> getAllEmployees() {
		try {
			List<CEmployee> pEmployees = new ArrayList<CEmployee>();

			employeeRepository.findAll().forEach(pEmployees::add);

			return new ResponseEntity<>(pEmployees, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employee/{id}")
	public ResponseEntity<CEmployee> getEmployeeById(@PathVariable("id") long id) {
		Optional<CEmployee> employeeData = employeeRepository.findById(id);
		if (employeeData.isPresent()) {
			return new ResponseEntity<>(employeeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/employee/create")
	public ResponseEntity<Object> createEmployee(@RequestBody CEmployee pEmployee) {
		try {
			CEmployee newEmployee = new CEmployee();
			newEmployee.setFirstName(pEmployee.getFirstName());
			newEmployee.setLastName(pEmployee.getLastName());
			newEmployee.setEmail(pEmployee.getEmail());
			newEmployee.setExtension(pEmployee.getExtension());
			newEmployee.setJobTitle(pEmployee.getJobTitle());
			newEmployee.setOfficeCode(pEmployee.getOfficeCode());
			newEmployee.setReportTo(pEmployee.getReportTo());
			CEmployee savedEmployee = employeeRepository.save(newEmployee);
			return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Employee: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/employee/update/{id}")
	public ResponseEntity<Object> updateEmployeeById(@PathVariable("id") long id, @RequestBody CEmployee pEmployee) {
		Optional<CEmployee> employeeData = employeeRepository.findById(id);
		if (employeeData.isPresent()) {
			CEmployee newEmployee = employeeData.get();
			newEmployee.setFirstName(pEmployee.getFirstName());
			newEmployee.setLastName(pEmployee.getLastName());
			newEmployee.setEmail(pEmployee.getEmail());
			newEmployee.setExtension(pEmployee.getExtension());
			newEmployee.setJobTitle(pEmployee.getJobTitle());
			newEmployee.setOfficeCode(pEmployee.getOfficeCode());
			newEmployee.setReportTo(pEmployee.getReportTo());
			CEmployee savedEmployee = employeeRepository.save(newEmployee);
			try {
				return new ResponseEntity<>(savedEmployee, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Employee:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Employee: " + id + "  for update.");
		}
	}

	@DeleteMapping("/employee/delete/{id}")
	public ResponseEntity<CEmployee> deleteEmployeeById(@PathVariable("id") long id) {
		try {
			employeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employee/first_or_lastname/{name}")
	public ResponseEntity<List<CEmployee>> findEmployeeByFirstOrLastName(@PathVariable String name) {
		try {
			List<CEmployee> vEmployee = employeeRepository.findEmployeeByFirstOrLastName(name);
			return new ResponseEntity<>(vEmployee, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/employee/first_or_lastname_pageable/{name}/{start}/{end}")
	public ResponseEntity<List<CEmployee>> findEmpoyeeByFirstOrLastNamePageable(@PathVariable String name, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CEmployee> vEmployee = employeeRepository.findEmployeeByFirstOrLastNamePageable(name, PageRequest.of(start, end));
			return new ResponseEntity<>(vEmployee, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/employee/first_or_lastname_desc/{name}/{start}/{end}")
	public ResponseEntity<List<CEmployee>> findEmpoyeeByFirstOrLastNameDesc(@PathVariable String name, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CEmployee> vEmployee = employeeRepository.findEmployeeByFirstOrLastNameDESC(name, PageRequest.of(start, end));
			return new ResponseEntity<>(vEmployee, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/employee/update_for_job_title/{job_title}")
	public int updateJobTitle(@PathVariable("job_title") String job_title) {
		return employeeRepository.updateJobTitle(job_title);
	}
}
