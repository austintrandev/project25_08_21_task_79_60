package com.shoppizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@RestController
public class CCustomerController {
	@Autowired
	private ICustomerRepository customerRepository;

	@GetMapping("/customer/first_or_lastname/{name}")
		public ResponseEntity<List<CCustomer>> findCustomerByFirstOrLastName(@PathVariable String name) {
			try {
				List<CCustomer> vCustomer = customerRepository.findCustomerByFirstOrLastName(name);
				return new ResponseEntity<>(vCustomer, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
		}
	}

	@GetMapping("/customer/city_or_state/{name}/{start}/{end}")
	public ResponseEntity<List<CCustomer>> findCustomerByCityOrState(@PathVariable String name,
			@PathVariable int start, @PathVariable int end) {
		try {
			List<CCustomer> vCustomer = customerRepository.findCustomerByCityOrState(name,
					PageRequest.of(start, end));
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/customer/country/{name}/{start}/{end}")
	public ResponseEntity<List<CCustomer>> findCustomerByCountry(@PathVariable String name,
			@PathVariable int start, @PathVariable int end) {
		try {
			List<CCustomer> vCustomer = customerRepository.findCustomerByCountryDESC(name,
					PageRequest.of(start, end));
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin
	@PutMapping("/customer/update_for_country/{country}")
	public int updateCountry(@PathVariable("country") String country) {
		return customerRepository.updateCountry(country);
	}
}
