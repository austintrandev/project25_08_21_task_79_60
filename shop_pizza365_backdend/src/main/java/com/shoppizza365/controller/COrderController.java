package com.shoppizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;
@RestController
public class COrderController {
	@Autowired
	private IOrderRepository orderRepository;

	@GetMapping("/order/status/{status}")
	public ResponseEntity<List<COrder>> findOrderByStatus(@PathVariable String status) {
		try {
			List<COrder> vOrder = orderRepository.findOrderByStatus(status);
			return new ResponseEntity<>(vOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/order/status_pageable/{status}/{start}/{end}")
	public ResponseEntity<List<COrder>> findOrderByStatusPageable(@PathVariable String status, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COrder> vOrder = orderRepository.findOrderByStatusPageable(status, PageRequest.of(start, end));
			return new ResponseEntity<>(vOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/order/status_desc/{status}/{start}/{end}")
	public ResponseEntity<List<COrder>> findOrderByStatusDESC(@PathVariable String status, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<COrder> vOrder = orderRepository.findOrderByStatusDESC(status, PageRequest.of(start, end));
			return new ResponseEntity<>(vOrder, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/order/update_for_status/{status}")
	public int updateStatus(@PathVariable("status") String status) {
		return orderRepository.updateStatus(status);
	}
}
