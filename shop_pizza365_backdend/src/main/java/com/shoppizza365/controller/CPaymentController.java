package com.shoppizza365.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@RestController
public class CPaymentController {
	@Autowired
	private IPaymentRepository paymentRepository;

	@GetMapping("/payment/checknumber/{checknumber}")
	public ResponseEntity<List<CPayment>> findPaymentByCheckNumber(@PathVariable String checknumber) {
		try {
			List<CPayment> vPayment = paymentRepository.findPaymentBycheckNumber(checknumber);
			return new ResponseEntity<>(vPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/payment/checknumber_pageable/{checknumber}/{start}/{end}")
	public ResponseEntity<List<CPayment>> findPaymentByCheckNumberPageable(@PathVariable String checknumber, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CPayment> vPayment = paymentRepository.findPaymentBycheckNumberPageable(checknumber, PageRequest.of(start, end));
			return new ResponseEntity<>(vPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/payment/checknumber_desc/{checknumber}/{start}/{end}")
	public ResponseEntity<List<CPayment>> findPaymentByCheckNumberDESC(@PathVariable String checknumber, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CPayment> vPayment = paymentRepository.findPaymentBycheckNumberDESC(checknumber, PageRequest.of(start, end));
			return new ResponseEntity<>(vPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/payment/update_for_checknumber/{checknumber}")
	public int updateCheckNumber(@PathVariable("checknumber") String checknumber) {
		return paymentRepository.updateCheckNumber(checknumber);
	}
}
