package com.shoppizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CProductController {
	@Autowired
	IProductRepository productRepository;

	@Autowired
	IProductLineRepository productLineRepository;

	@GetMapping("/products")
	public ResponseEntity<List<CProduct>> getAllProducts() {
		try {
			List<CProduct> pProducts = new ArrayList<CProduct>();

			productRepository.findAll().forEach(pProducts::add);

			return new ResponseEntity<>(pProducts, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/product/{id}")
	public ResponseEntity<CProduct> getProductById(@PathVariable("id") long id) {
		Optional<CProduct> productData = productRepository.findById(id);
		if (productData.isPresent()) {
			return new ResponseEntity<>(productData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/productline/create/{id}")
	public ResponseEntity<Object> createProductLine(@PathVariable("id") Long id, @RequestBody CProduct pProduct) {
		Optional<CProductLine> productLineData = productLineRepository.findById(id);
		if (productLineData.isPresent()) {
			try {
				CProduct newProduct = new CProduct();
				newProduct.setProductCode(pProduct.getProductCode());
				newProduct.setBuyPrice(pProduct.getBuyPrice());
				newProduct.setProductDescription(pProduct.getProductDescription());
				newProduct.setProductName(pProduct.getProductName());
				newProduct.setProductScale(pProduct.getProductScale());
				newProduct.setProductVendor(pProduct.getProductVendor());
				newProduct.setQuantityInStock(pProduct.getQuantityInStock());
				newProduct.setProductLine(productLineData.get());
				CProduct savedProduct = productRepository.save(newProduct);
				return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/product/update/{id}")
	public ResponseEntity<Object> updateProductById(@PathVariable("id") long id,
			@RequestBody CProduct pProduct) {
		Optional<CProduct> productData = productRepository.findById(id);
		if (productData.isPresent()) {
			CProduct newProduct = productData.get();
			newProduct.setProductCode(pProduct.getProductCode());
			newProduct.setBuyPrice(pProduct.getBuyPrice());
			newProduct.setProductDescription(pProduct.getProductDescription());
			newProduct.setProductName(pProduct.getProductName());
			newProduct.setProductScale(pProduct.getProductScale());
			newProduct.setProductVendor(pProduct.getProductVendor());
			newProduct.setQuantityInStock(pProduct.getQuantityInStock());
			CProduct savedProduct = productRepository.save(newProduct);
			try {
				return new ResponseEntity<>(savedProduct, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Product:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Product: " + id + "  for update.");
		}
	}

	@DeleteMapping("/product/delete/{id}")
	public ResponseEntity<CProduct> deleteProductById(@PathVariable("id") long id) {
		try {
			productRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/product/product_name/{name}")
	public ResponseEntity<List<CProduct>> findProductByProductName(@PathVariable String name) {
		try {
			List<CProduct> vProduct = productRepository.findProductByName(name);
			return new ResponseEntity<>(vProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/product/product_name_pageable/{name}/{start}/{end}")
	public ResponseEntity<List<CProduct>> findProductByProductNamePageable(@PathVariable String name, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CProduct> vProduct = productRepository.findProductByNamePageable(name, PageRequest.of(start, end));
			return new ResponseEntity<>(vProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/product/product_name_desc/{name}/{start}/{end}")
	public ResponseEntity<List<CProduct>> findProductByProductNameDESC(@PathVariable String name, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CProduct> vProduct = productRepository.findProductByNameDESC(name, PageRequest.of(start, end));
			return new ResponseEntity<>(vProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/product/update_for_product_name/{name}")
	public int updateProductName(@PathVariable("name") String name) {
		return productRepository.updateProductName(name);
	}
}
